## Min-Max scheduler

by Christian Bermejo and Harold Bolingot

## Documentation

### Prerequisites
The program uses system libraries of Python 2, and thus, Python 2 is required.

### How to use:
1. Execute the program by typing 'python sched_algo.py' into the command line
2. Enter the number of processes, as well as their corresponding CPU bursts, arrival time and priority
3. Choose which scheduling algorithm to execute
4. Depending on the chosen algorithm, they might prompt you to provide more input such as custom quantum time, process ratio and/or threshold value
5. Results such as the processes and their corresponding values of waiting time, response time and turnaround time as displayed on the console.


### Overview
1. structures.py - contains the classes which make up the Process object
2. sched_algo.py - contains the functions of the scheduling algorithms as well as the function which invokes the menu when the program is run


## Special Thanks
A quick mention to Iury Alves; the group has built upon his framework of an object-oriented approach to the classic scheduling algorithms.
