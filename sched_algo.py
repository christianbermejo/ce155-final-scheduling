'''
Original Source Code by Iury Alves
https://gist.github.com/IuryAlves

Modified for English readability
'''
import os
import random
from structures import *
import math
import copy


class SchedulingAlgorithm(object):
	"""
	Base class for all algorithms
	scaling containing the methods
	and common attributes
	"""
	def __init__(self, processList):
		self.processList = processList
		self.processList_len = len(processList)
		self.turnaround_per_process = {p.id: 0 for p in self.processList}
		self.processTuple = tuple(self.processList)
		self.waiting_times = {p.id: set() for p in self.processList}
		self.waiting_times_count = {
			p.id: -(p.pcb.arriveTime) for p in self.processList
		}
		self.response_times = {
			p.id: p.pcb.responseTime for p in self.processList
		}

	@classmethod
	def __repr__(cls):
		return cls.__name__

	def _clean_waiting_times_count(self):
		"""
		Reset the timeout counters
		"""
		self.waiting_times_count = {
			p.id: -(p.pcb.arriveTime) for p in self.processTuple
		}

	def get_turnaround_time(self, process_id):
		"""
		Returns the turnaround time of a process
		defined by its id
		"""
		return self.turnaround_per_process[process_id]

	def get_average_turnaround_time(self):
		"""
		Returns the average time turnaround
		processes
		"""
		return sum(self.turnaround_per_process.values()) / self.processList_len

	def _set_turnaround_time(self, time, process):
		"""
		Sets the turnaround of a process
		"""
		self.turnaround_per_process[process.id] = (time - process.pcb.arriveTime)

	def sort_processes(self):
		"""
		Defines how the processes will be prioritized
		It should be implemented in a sub class
		"""
		pass

	def _increment_response_time(self, time, process):
		"""
		Increase the response time of a process
		"""
		self.response_times[process.id] += time

	def get_response_time(self, process_id):
		"""
		Returns the response time of a process
		"""
		return float(self.response_times[process_id])

	def _increment_wait_time(self, time, actual_process):
		"""
		Increase the wait time for all processes
		except for what is running
		"""
		for process in self.processList:
			if process != actual_process:
				self.waiting_times_count[process.id] += time
			else:
				self.waiting_times[actual_process.id] = self.waiting_times_count[process.id]

	def get_waiting_time(self, process_id):
		"""
		Returns the waiting time of a process
		"""
		return float(self.waiting_times[process_id])

	def get_average_waiting_time(self):
		"""
		Returns the average waiting time for all
		processes
		"""
		return sum([self.get_waiting_time(p.id) for p in self.processTuple]) / self.processList_len

	def run(self):
		"""
		Runs the scheduling algorithm
		"""
		time = 0
		newtoready = 5 # default value for first newtoready
		for index, process in enumerate(self.processList):
			self._increment_wait_time(time, process)
			time += newtoready # to account for newtoready
			self._increment_response_time(time, process)
			time += process.pcb.burst
			self._set_turnaround_time(time, process)
			time -= newtoready # to account for newtoready

class FCFS(SchedulingAlgorithm):
	"""
	FCFS
	First Come First Serve
	Non-Preemptive
	"""
	def __init__(self, processList):
		super(FCFS, self).__init__(processList)
		self.sort_processes()

	def sort_processes(self):
		"""
		Ordering processes by arrival time
		"""
		return self.processList.sort(key=lambda process: process.pcb.arriveTime)

	def _increment_wait_time(self, time, process):
		self.waiting_times_count[process.id] += time

	def _increment_response_time(self, time, process):
		self.response_times[process.id] += time

	def get_waiting_time(self, process_id):
		return float(self.waiting_times_count[process_id])

class SJF(SchedulingAlgorithm):
	"""
	First run processes with smaller burst.
	If the processes arrive at different times,
	then commands [1:] and by the time the first burst arrival time for
	Non-Preemptive
	"""
	def __init__(self, processList):
		super(SJF, self).__init__(processList)
		self.sort_processes()

	def sort_processes(self):
		"""
		Ordering processes at the lowest burst time
		"""
		equals = True
		for process in self.processList[1:]:
			if process.pcb.arriveTime != self.processList[0].pcb.arriveTime:
				equals = False

		if equals:
			self.processList.sort(key=lambda process: process.pcb.burst)
		else:
			self.processList.sort(key=lambda process: process.pcb.arriveTime)
			self.processList = [self.processList.pop(0)]
			new_list = list(self.processTuple[1:])
			new_list.sort(key=lambda process: process.pcb.burst)
			self.processList.extend(new_list)

	def _increment_wait_time(self, time, process):
		self.waiting_times_count[process.id] += time

	def get_waiting_time(self, process_id):
		return float(self.waiting_times_count[process_id])

class RoundRobin(SchedulingAlgorithm):
	"""
	Set quantum time for each process run
	Standard is 20
	Preemptive
	"""
	def __init__(self, processList, quantum=20, break_on_cycle=False):
		super(RoundRobin, self).__init__(processList)
		self.sort_processes()
		self.quantum = quantum
		self.break_on_cycle = break_on_cycle
		self.index = 0
		self.newtoready = 5

	def _context_change(self, process):
		if self.index < len(self.processList) - 1:
			self.index += 1
			return self.processList[self.index]
		else:
			self.index = 0
			return self.processList[self.index]

	def _process_finished(self, time, process, newtoready):
		self.processList.remove(process)
		self.index -= 1
		self._set_turnaround_time(time, process, newtoready)

	def _set_turnaround_time(self, time, process, newtoready):
		self.turnaround_per_process[process.id] = (time + newtoready)

	def run(self):
		time = 0
		cycle = 0
		loop = 0 # used to check for one-run loop to account for newtoready
		process = self.processList[0]
		beforeresponse = self.newtoready # this is the time after newtoready but just before first cpu burst
		while self.processList:
			# if loop < origprocesslen:
			if loop < self.processList_len:
				self._increment_response_time(beforeresponse, process)

			if process.pcb.burst <= self.quantum:
				time += process.pcb.burst
				beforeresponse += process.pcb.burst
				self._increment_wait_time(process.pcb.burst, process)
				self._process_finished(time, process, self.newtoready)

				if not self.processList:
					break
			else:
				process.pcb.burst -= self.quantum
				time += self.quantum
				self._increment_wait_time(self.quantum, process)
				# if loop < origprocesslen:
				if loop < self.processList_len:
					beforeresponse += self.quantum

			if self.break_on_cycle:
				cycle += 1
				self._set_turnaround_time(time, process, self.newtoready)

			process = self._context_change(process)
			loop += 1
			if self.break_on_cycle and cycle == len(self.processTuple):
				break

class MinMax(SchedulingAlgorithm):
	"""
	"""
	def __init__(self, processList):
		super(MinMax, self).__init__(processList)
		self.threshold = float(51)
		self.totalProcess = self.processList_len
		self.totProcessList = list(self.processList)

	def sort_processes(self):
		""" Ordering processes at the lowest burst time"""
		return self.totProcessList.sort(key=lambda process: process.pcb.burst)

	def _process_finished(self, time, process):
		self._set_turnaround_time(time, process)
		self.totProcessList.remove(process)

	def _increment_wait_time(self, time, actual_process):
		"""Increase the wait time for all processes except for what is running
		Removed processes with duplicated IDs when increasing wait time
		to disallow duplicate addition
		"""
		output = []
		seen = set()
		# attempt to remove processes with duplicate IDs for waiting time addition
		for process in self.totProcessList:
			if process.id not in seen:
				output.append(process.id)
				seen.add(process.id)
		for process in output:
			if process != actual_process.id:
				self.waiting_times_count[process] += time
			else:
				self.waiting_times[actual_process.id] = self.waiting_times_count[process]

	def _set_turnaround_time(self, time, process):
		self.turnaround_per_process[process.id] = (time)

	def get_average_waiting_time(self):
		""" Returns the average waiting time for all processes
		"""
		return sum([self.get_waiting_time(p.id) for p in self.processTuple]) / self.processList_len

	def get_waiting_time(self, process_id):
		return float(self.waiting_times_count[process_id])

	def _divide_large_process(self, process):
		''' divide large process into two parts of 1/4, 3/4 or 1:3 ratio'''
		new_pcb_burst_1 = math.ceil(process.pcb.burst / 4)
		new_pcb_burst_2 = process.pcb.burst - new_pcb_burst_1
		arriveTime = process.pcb.arriveTime
		priority = process.pcb.priority
		new_id = process.id
		aproc = Process(PCB(new_pcb_burst_1, arriveTime, priority), new_id, "b")
		bproc = Process(PCB(new_pcb_burst_2, arriveTime, priority), new_id, "c")
		self.totProcessList.append(aproc)
		self.totProcessList.append(bproc)
		self.totProcessList.remove(process)

	def run(self):
		time = 0
		mmloop = 1
		while self.totProcessList:
			self.sort_processes()
			# checks if the process is the first process before response
			firstResponse = 0
			if mmloop % 4 != 0:
				# execute first (i.e. smallest) process in ready queue
				process = self.totProcessList[0]
				if firstResponse != 0:
					self._increment_response_time(time, process)

				self._increment_wait_time(process.pcb.burst, process)
				time += process.pcb.burst
				self._process_finished(time, process)

				if not self.totProcessList:
					break

				# switch to last (i.e. largest) process
				end = len(self.totProcessList) - 1
				process = self.totProcessList[end]
				# check if largest job burst time is less
				# than threshold
				if process.pcb.burst < self.threshold:
					'''
					check for other processes of duplicate ID, if there are, it means this can be firstResponse
					'''
					sameIDprocesses = [p for p in self.totProcessList if (p.id == process.id and p.pcb.burst < process.pcb.burst)]
					if sameIDprocesses and firstResponse == 0:
						self._increment_response_time(time, process)
						firstResponse = 1

					self._increment_wait_time(process.pcb.burst, process)
					time += process.pcb.burst
					self._process_finished(time, process)
				else:
					# break split large process into two parts
					self._divide_large_process(process)
					if not self.totProcessList:
						break
			else:
				# execute middle process from ready queue
				mid = int(len(self.totProcessList)) / 2
				process = self.totProcessList[mid]
				self._increment_response_time(time, process)
				self._increment_wait_time(process.pcb.burst, process)
				time += process.pcb.burst
				self._process_finished(time, process)
			mmloop += 1

class MinMaxModified(SchedulingAlgorithm):
	"""
	"""
	def __init__(self, processList, ratio1=1, ratio2=3, threshold=51):
		super(MinMaxModified, self).__init__(processList)
		self.threshold = float(51)
		self.totalProcess = self.processList_len
		self.totProcessList = list(self.processList)
		self.ratio1 = ratio1
		self.ratio2 = ratio2
		self.threshold = threshold

	def sort_processes(self):
		""" Ordering processes at the lowest burst time"""
		return self.totProcessList.sort(key=lambda process: process.pcb.burst)

	def _process_finished(self, time, process):
		self._set_turnaround_time(time, process)
		self.totProcessList.remove(process)

	def _increment_wait_time(self, time, actual_process):
		"""Increase the wait time for all processes except for what is running
		Removed processes with duplicated IDs when increasing wait time
		to disallow duplicate addition
		"""
		output = []
		seen = set()
		for process in self.totProcessList:
			if process.id not in seen:
				output.append(process.id)
				seen.add(process.id)
		for process in output:
			if process != actual_process.id:
				self.waiting_times_count[process] += time
			else:
				self.waiting_times[actual_process.id] = self.waiting_times_count[process]

	def _set_turnaround_time(self, time, process):
		self.turnaround_per_process[process.id] = (time)

	def get_average_waiting_time(self):
		""" Returns the average waiting time for all processes
		"""
		return sum([self.get_waiting_time(p.id) for p in self.processTuple]) / self.processList_len

	def get_waiting_time(self, process_id):
		return float(self.waiting_times_count[process_id])

	def _divide_large_process(self, process):
		''' divide large process into two parts of prescribed ratio'''
		new_pcb_burst_1 = math.ceil(self.ratio1 * process.pcb.burst / (self.ratio1 + self.ratio2))
		new_pcb_burst_2 = process.pcb.burst - new_pcb_burst_1
		arriveTime = process.pcb.arriveTime
		priority = process.pcb.priority
		new_id = process.id
		aproc = Process(PCB(new_pcb_burst_1, arriveTime, priority), new_id, "b")
		bproc = Process(PCB(new_pcb_burst_2, arriveTime, priority), new_id, "c")
		self.totProcessList.append(aproc)
		self.totProcessList.append(bproc)
		self.totProcessList.remove(process)

	def run(self):
		time = 0
		mmloop = 1

		print 'Ratio: %s : %s' % (self.ratio1, self.ratio2)
		print 'Threshold: %s' % (self.threshold)

		while self.totProcessList:
			self.sort_processes()
			# checks if the process is the first process before response
			firstResponse = 0
			if mmloop % 4 != 0:
				# execute first (i.e. smallest) process in ready queue
				process = self.totProcessList[0]
				if firstResponse != 0:
					self._increment_response_time(time, process)

				self._increment_wait_time(process.pcb.burst, process)
				time += process.pcb.burst
				self._process_finished(time, process)

				if not self.totProcessList:
					break

				# switch to last (i.e. largest) process
				end = len(self.totProcessList) - 1
				process = self.totProcessList[end]
				# check if largest job burst time is less
				# than threshold
				if process.pcb.burst < self.threshold:
					'''
					check for other processes of duplicate ID, if there are, it means this can be firstResponse
					'''
					sameIDprocesses = [p for p in self.totProcessList if (p.id == process.id and p.pcb.burst < process.pcb.burst)]
					if sameIDprocesses and firstResponse == 0:
						self._increment_response_time(time, process)
						firstResponse = 1

					self._increment_wait_time(process.pcb.burst, process)
					time += process.pcb.burst
					self._process_finished(time, process)
				else:
					# break split large process into two parts
					self._divide_large_process(process)
					if not self.totProcessList:
						break
			else:
				# execute middle process from ready queue
				mid = int(len(self.totProcessList)) / 2
				process = self.totProcessList[mid]
				self._increment_response_time(time, process)
				self._increment_wait_time(process.pcb.burst, process)
				time += process.pcb.burst
				self._process_finished(time, process)
			mmloop += 1

def main():
	menu = Menu([FCFS, SJF, RoundRobin, MinMax, MinMaxModified])
	menu.quantity = int(input("Enter the number of processes: "))
	os.system("clear")

	scheduling_algorithms_map = {
		1: FCFS,
		2: SJF,
		3: RoundRobin,
		4: MinMax,
		5: MinMaxModified
	}

	for i in range(menu.quantity):
		burst = float(input("Enter burst time of process[%i]: " % (i + 1)))
		# burst = int(input("Enter burst time of process[%i]: " % (i + 1)))
		os.system("clear")
		arriveTime = int(input("Enter arrival time of process[%i]: " % (i + 1)))
		os.system("clear")
		priority = int(input("Enter priority of process[%i]: " % (i + 1)))
		os.system("clear")

		menu.processList.append(Process(PCB(burst, arriveTime, priority), random.randint(0,200), "a"))

	menu.print_options()
	Scheduler = None
	while Scheduler is None:
		menu.scheduling_algorithm = int(input('Enter the scheduling algorithm:'))
		os.system("clear")
		Scheduler = scheduling_algorithms_map.get(menu.scheduling_algorithm)

		if Scheduler is None:
			print TermColors.RED + "Enter a number from 1 - 5" + TermColors.ENDC


	scheduler = Scheduler(menu.processList)
	if isinstance(scheduler, RoundRobin):
		quantum = int(input("Enter quantum time: "))
		os.system("clear")
		scheduler.quantum = quantum

	if isinstance(scheduler, MinMaxModified):
		ratio1 = int(input("Enter first ratio number (default 1): "))
		os.system("clear")
		ratio2 = int(input("Enter second ratio number (default 3): "))
		os.system("clear")
		threshold = int(input("Enter threshold value (default 51): "))
		scheduler.ratio1 = ratio1
		scheduler.ratio2 = ratio2
		scheduler.threshold = threshold

	menu.print_processes()
	print "Chosen algorithm: " + TermColors.RED + str(scheduler) + TermColors.ENDC + "\n"
	scheduler.run()

	menu.print_metrics(scheduler)
	menu.print_averages(scheduler)
	print "\n"

if __name__ == '__main__':
	main()
