""" Import Process and BJFProcess classes from process module """
from process import Process
from process import BJFProcess

""" Function for creating a list of five Process objects """
def initProcesses():
    processes = []
    # TO-DO random int generator for CPU bursts
    # Process(name, burst, arrival, init)
    processes.append(Process("P1", 11, 0, 0))
    processes.append(Process("P2", 9,  4, 0))
    processes.append(Process("P3", 12, 5, 0))
    processes.append(Process("P4", 15, 8, 0))
    processes.append(Process("P5", 8, 11, 0))
    return processes

""" Function for displaying output """
def displayOutput(processes):
    for index in range(len(processes)):
        print(processes[index].name + " response time = " + str(processes[index].responseTime + processes[index].init))
    for index in range(len(processes)):
        print(processes[index].name + " waiting time = " + str(processes[index].waitingTime))
    for index in range(len(processes)):
        print(processes[index].name + " turnaround time = " + str(processes[index].turnaroundTime))

""" FCFS (First-Come, First-Served) CPU Scheduling Algorithm Simulation """
def FCFS():
    """ Initialize process list """
    processes = initProcesses()

    """ Sort processes list according to arrival time in ascending order """
    processes.sort(key = lambda x: (x.arrival))

    """ Begin FCFS CPU Scheduling Algorithm Simulation """
    for indexA in range(len(processes)):
        processes[indexA].cpuResponse = True
        processes[indexA].currentlyRunning = True
        while (processes[indexA].burstLeft > 0):
            processes[indexA].burstLeft -= 1
            processes[indexA].turnaroundTime += 1
            for indexB in range(len(processes)):
                if (processes[indexB].currentlyRunning == False):
                    processes[indexB].waitingTime += 1
                    processes[indexB].turnaroundTime += 1
                    if (processes[indexB].cpuResponse == False):
                        processes[indexB].responseTime += 1
            # DEBUG print(processes[indexA].name + " executed CPU burst " + str(processes[indexA].burstLeft))

    """ Output results in console window """
    displayOutput(processes)

print("/====== FCFS ======/")
FCFS()
print("\n")

""" SJF (Shortest Job First) Non-Preemptive CPU Scheduling Algorithm Simulation """
def SJF_NP():
    """ Initialize process list """
    processes = initProcesses()

    """ Sort processes list according to number of cpu bursts in ascending order """
    processes.sort(key = lambda x: x.burst)

    """ Begin SJF CPU Scheduling Algorithm Simulation """
    for indexA in range(len(processes)):
        processes[indexA].cpuResponse = True
        processes[indexA].currentlyRunning = True
        while (processes[indexA].burstLeft > 0):
            processes[indexA].burstLeft -= 1
            processes[indexA].turnaroundTime += 1
            for indexB in range(len(processes)):
                if (processes[indexB].currentlyRunning == False):
                    processes[indexB].waitingTime += 1
                    processes[indexB].turnaroundTime += 1
                    if (processes[indexB].cpuResponse == False):
                        processes[indexB].responseTime += 1
            # DEBUG print(processes[indexA].name + " executed CPU burst " + str(processes[indexA].burstLeft))

    """ Output results in console window """
    displayOutput(processes)

print("/===== SJF NP =====/")
SJF_NP()
print("\n")

""" Round Robin CPU Scheduling Algorithm Simulation """
def RR(quantumTime):
    """ Initialize process list """
    processes = initProcesses()
