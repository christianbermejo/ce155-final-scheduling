class PCB(object):

	def __init__(self, burst, arriveTime, priority):
		self.burst = float(burst)
		# self.burst = burst
		self.arriveTime  = arriveTime
		self.priority = priority
		self.responseTime = 0	#default time from New-Ready state

	def __repr__(self):
		return TermColors.BLUE + "Arrive Time: %s \nPriority: %s \nResponse Time: %s \nBurst Time: %s \n" % tuple(self.__dict__.values()) + TermColors.ENDC


class Menu(object):
	def __init__(self, algorithms):
		self.processList = []
		self.algorithms = algorithms

	def print_processes(self):
		print TermColors.YELLOW + "Processes \n" + TermColors.ENDC
		for process in self.processList:
			print process

	@staticmethod
	def print_metrics(scheduler):
		for process in scheduler.processTuple:
			print "Waiting Time for Process[%i] = %4.2f" % (process.id, scheduler.get_waiting_time(process.id))
			print "Response Time for Process[%i] = %4.2f" % (process.id, scheduler.get_response_time(process.id))
			print "Turnaround Time for Process[%i] = %4.2f" % (process.id, scheduler.get_turnaround_time(process.id))
			print "\n"

	@staticmethod
	def print_averages(scheduler):
		print "Average Waiting Time = %4.2f" % scheduler.get_average_waiting_time()
		print "Average Turnaround Time = %4.2f" % scheduler.get_average_turnaround_time()

	def print_options(self):
		for index, algorithm in enumerate(self.algorithms):
			print TermColors.BLUE + str(index + 1) + TermColors.ENDC + " : " + TermColors.RED + algorithm.__name__ + TermColors.ENDC

class TermColors(object):
	BLUE = '\033[94m'
	GREEN = '\033[92m'
	YELLOW = '\033[93m'
	RED = '\033[91m'
	ENDC = '\033[0m'

class Process(object):
	def __init__(self, pcb, _id, _subid):
		self.pcb =  pcb
		self.id = _id
		self.subid = _subid

	def __repr__(self):
		return TermColors.BLUE + "Id: %s \n" % self.id + TermColors.ENDC +  self.pcb.__repr__() + TermColors.GREEN + "---------------" + TermColors.ENDC
